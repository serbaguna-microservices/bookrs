## BookRS Microservice (TK Adpro A7)
[![pipeline status](https://gitlab.com/serbaguna-microservices/bookrs/badges/master/pipeline.svg)](https://gitlab.com/serbaguna-microservices/bookrs/-/commits/master)
[![coverage report](https://gitlab.com/serbaguna-microservices/bookrs/badges/master/coverage.svg)](https://gitlab.com/serbaguna-microservices/bookrs/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=serbaguna-microservices_bookrs&metric=alert_status)](https://sonarcloud.io/dashboard?id=serbaguna-microservices_bookrs)

Dibuat oleh Basyira Sabita - 1906400034 sebagai bagian dari [Tugas Kelompok Adpro Kelompok A7](https://gitlab.com/serbaguna-microservices)

## SonarCloud
Proyek ini juga di-scan untuk Code Smells, Bugs, dan Vulnerabilities dengan SonarCloud yang bisa dilihat [link berikut](https://sonarcloud.io/dashboard?id=serbaguna-microservices_bookrs)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=serbaguna-microservices_bookrs)](https://sonarcloud.io/dashboard?id=serbaguna-microservices_bookrs)

## Links dan Line ID
Microservice ini di deploy di https://serbaguna-bookrs.herokuapp.com/

Line ID OA: `@109tpdsd`
