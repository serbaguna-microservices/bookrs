package csui.serbagunabot.bookrs.core;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class RSDoctor {
    private String kode = "";
    private String rs;
    private String dept;
    private String nama;
    private String jadwal1;
    private String jadwal2;
    private DayOfWeek hari1;
    private DayOfWeek hari2;
    private LocalDate apptDate;
    private String apptDay;
    private String apptClock;

    public RSDoctor() {}

    /**
     * constructor for defining RSDoctor with exact parameters.
     */
    public RSDoctor(String rs, String dept, String nama,
                    String jadwal1, String jadwal2,
                    DayOfWeek hari1, DayOfWeek hari2) {
        this.rs = rs;
        this.dept = dept;
        this.nama = nama;
        this.jadwal1 = jadwal1;
        this.jadwal2 = jadwal2;
        this.hari1 = hari1;
        this.hari2 = hari2;

    }

    public String getDept() {
        return this.dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getRs() {
        return this.rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public void makeKode(String attr) {
        this.kode += attr;
    }

    public String getKode() {
        return this.kode;
    }

    public String getNama() {
        return nama;
    }

    public void setKode(String attr) {
        this.kode = attr;
    }

    public String getJadwal1() {
        return this.jadwal1;
    }

    public String getJadwal2() {
        return this.jadwal2;
    }

    public DayOfWeek getHari1() {
        return this.hari1;
    }

    public DayOfWeek getHari2() {
        return this.hari2;
    }

    public LocalDate getApptDate() {
        return this.apptDate;
    }

    public String getApptDay() {
        return this.apptDay;
    }

    public String getApptClock() {
        return this.apptClock;
    }

    public void setApptDate(LocalDate apptDate, String apptDay, String apptClock) {
        this.apptDate = apptDate;
        this.apptDay = apptDay;
        this.apptClock = apptClock;
    }
}