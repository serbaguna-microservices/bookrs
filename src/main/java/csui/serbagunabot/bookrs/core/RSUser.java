package csui.serbagunabot.bookrs.core;

import csui.serbagunabot.bookrs.states.RSState;
import csui.serbagunabot.bookrs.states.RSStateGenerator;

//@Setter
//@Getter
public class RSUser {

    private RSState currentState;
    private String rumahSakit;
    private String departemen;
    private String namaPasien;
    private String namaDokter;
    private RSDoctor rsDoctor;

    public RSUser() {
        this.rsDoctor = new RSDoctor();
        this.currentState = RSStateGenerator.initializingRSState(this);
    }

    public RSState getCurrentState() {
        return this.currentState;
    }

    public void setCurrentState(RSState state) {
        this.currentState = state;
    }

    public String getRumahSakit() {
        return this.rumahSakit;
    }

    public void setRumahSakit(String rumahSakit) {
        this.rumahSakit = rumahSakit;
    }

    public String getDepartemen() {
        return this.departemen;
    }

    public void setDepartemen(String departemen) {
        this.departemen = departemen;
    }

    public String getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        this.namaPasien = namaPasien;
    }

    public String getNamaDokter() {
        return namaDokter;
    }

    public void setNamaDokter(String namaDokter) {
        this.namaDokter = namaDokter;
    }

    public RSDoctor getRsDoctor() {
        return this.rsDoctor;
    }

    public void setRsDoctor(RSDoctor rsDoctor) {
        this.rsDoctor = rsDoctor;
    }

    public String respond(String cleanMessage) {
        return currentState.getHandleMessage(cleanMessage);
    }


}
