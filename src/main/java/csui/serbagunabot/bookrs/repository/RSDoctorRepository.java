package csui.serbagunabot.bookrs.repository;

import csui.serbagunabot.bookrs.core.RSDoctor;

public interface RSDoctorRepository {
    RSDoctor findByKode(String kode);

    void saveDoctor(String kode, RSDoctor rsDoctor);
}
