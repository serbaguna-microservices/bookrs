package csui.serbagunabot.bookrs.repository;

import csui.serbagunabot.bookrs.core.RSDoctor;
import java.util.HashMap;
import java.util.Map;

public class RSDoctorRepositoryImpl implements RSDoctorRepository {

    private static RSDoctorRepositoryImpl doctorRepository = new RSDoctorRepositoryImpl();

    Map<String, RSDoctor> mapDoctor = new HashMap<>();

    private RSDoctorRepositoryImpl() {

    }

    public static RSDoctorRepositoryImpl getDoctorRepository() {
        return doctorRepository;
    }

    @Override
    public RSDoctor findByKode(String kode) {
        return mapDoctor.get(kode);
    }

    @Override
    public void saveDoctor(String kode, RSDoctor rsDoctor) {
        mapDoctor.put(kode, rsDoctor);
    }
}
