package csui.serbagunabot.bookrs.repository;

import csui.serbagunabot.bookrs.core.RSUser;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class RSUserRepositoryImpl implements RSUserRepository {

    Map<String, RSUser> mapUser = new HashMap<>();

    @Override
    public RSUser findById(String userId) {
        return mapUser.get(userId);
    }

    @Override
    public RSUser createUserFromId(String userId) {
        mapUser.put(userId, new RSUser());
        return findById(userId);
    }
}
