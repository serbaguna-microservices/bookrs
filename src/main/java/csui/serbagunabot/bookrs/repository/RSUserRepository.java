package csui.serbagunabot.bookrs.repository;

import csui.serbagunabot.bookrs.core.RSUser;

public interface RSUserRepository {
    RSUser findById(String userId);

    RSUser createUserFromId(String userId);
}
