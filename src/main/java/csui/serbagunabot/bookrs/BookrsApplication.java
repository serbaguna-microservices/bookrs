package csui.serbagunabot.bookrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookrsApplication {
    public static void main(String[] args) {
        SpringApplication.run(BookrsApplication.class, args);
    }
}
