package csui.serbagunabot.bookrs;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.repository.RSDoctorRepository;
import csui.serbagunabot.bookrs.repository.RSDoctorRepositoryImpl;
import java.time.DayOfWeek;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class BookRSInitializer {

    private RSDoctorRepository rsDoctorRepository = RSDoctorRepositoryImpl.getDoctorRepository();

    private static final String JADWAL11 = "Senin 08.00-11.00";
    private static final String JADWAL12 = "Rabu 13.00-16.00";
    private static final String JADWAL21 = "Selasa 08.00-11.00";
    private static final String JADWAL22 = "Kamis 13.00-16.00";
    private static final String JADWAL31 = "Jumat 08.00-11.00";
    private static final String JADWAL32 = "Sabtu 13.00-16.00";
    private static final String DEPT_GIGI = "Gigi dan Bedah Mulut";

    /**
     * Initializer.
     */
    @PostConstruct
    public void init() {

        RSDoctor rsDoctorAU1 = new RSDoctor("RS A", "Umum", "dr. Astuti",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("AU1", rsDoctorAU1);
        RSDoctor rsDoctorAU2 = new RSDoctor("RS A", "Umum", "dr. Budiman",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("AU2", rsDoctorAU2);
        RSDoctor rsDoctorAU3 = new RSDoctor("RS A", "Umum", "dr. Carita",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("AU3", rsDoctorAU3);
        RSDoctor rsDoctorAG1 = new RSDoctor("RS A", DEPT_GIGI, "drg. Gilang",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("AG1", rsDoctorAG1);
        RSDoctor rsDoctorAG2 = new RSDoctor("RS A", DEPT_GIGI, "drg. Heni",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("AG2", rsDoctorAG2);
        RSDoctor rsDoctorAG3 = new RSDoctor("RS A", DEPT_GIGI, "drg. Indah",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("AG3", rsDoctorAG3);
        RSDoctor rsDoctorAT1 = new RSDoctor("RS A", "THT", "dr. Diandra",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("AT1", rsDoctorAT1);
        RSDoctor rsDoctorAT2 = new RSDoctor("RS A", "THT", "dr. Edison",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("AT2", rsDoctorAT2);
        RSDoctor rsDoctorAT3 = new RSDoctor("RS A", "THT", "dr. Fumala",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("AT3", rsDoctorAT3);

        RSDoctor rsDoctorBU1 = new RSDoctor("RS B", "Umum", "dr. Alika",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("BU1", rsDoctorBU1);
        RSDoctor rsDoctorBU2 = new RSDoctor("RS B", "Umum", "dr. Bertha",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("BU2", rsDoctorBU2);
        RSDoctor rsDoctorBU3 = new RSDoctor("RS B", "Umum", "dr. Charlie",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("BU3", rsDoctorBU3);
        RSDoctor rsDoctorBG1 = new RSDoctor("RS B", DEPT_GIGI, "drg. Gunawan",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("BG1", rsDoctorBG1);
        RSDoctor rsDoctorBG2 = new RSDoctor("RS B", DEPT_GIGI, "drg. Hani",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("BG2", rsDoctorBG2);
        RSDoctor rsDoctorBG3 = new RSDoctor("RS B", DEPT_GIGI, "drg. Imara",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("BG3", rsDoctorBG3);
        RSDoctor rsDoctorBT1 = new RSDoctor("RS B", "THT", "dr. Dilan",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("BT1", rsDoctorBT1);
        RSDoctor rsDoctorBT2 = new RSDoctor("RS B", "THT", "dr. Endah",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("BT2", rsDoctorBT2);
        RSDoctor rsDoctorBT3 = new RSDoctor("RS B", "THT", "dr. Fitri",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("BT3", rsDoctorBT3);

        RSDoctor rsDoctorCU1 = new RSDoctor("RS C", "Umum", "dr. Amanda",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("CU1", rsDoctorCU1);
        RSDoctor rsDoctorCU2 = new RSDoctor("RS C", "Umum", "dr. Bambang",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("CU2", rsDoctorCU2);
        RSDoctor rsDoctorCU3 = new RSDoctor("RS C", "Umum", "dr. Carla",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("CU3", rsDoctorCU3);
        RSDoctor rsDoctorCG1 = new RSDoctor("RS C", DEPT_GIGI, "drg. Gilang",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("CG1", rsDoctorCG1);
        RSDoctor rsDoctorCG2 = new RSDoctor("RS C", DEPT_GIGI, "drg. Heni",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("CG2", rsDoctorCG2);
        RSDoctor rsDoctorCG3 = new RSDoctor("RS C", DEPT_GIGI, "drg. Indah",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("CG3", rsDoctorCG3);
        RSDoctor rsDoctorCT1 = new RSDoctor("RS C", "THT", "dr. Doni",
                JADWAL11, JADWAL12, DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY);
        rsDoctorRepository.saveDoctor("CT1", rsDoctorCT1);
        RSDoctor rsDoctorCT2 = new RSDoctor("RS C", "THT", "dr. Emma",
                JADWAL21, JADWAL22, DayOfWeek.TUESDAY, DayOfWeek.THURSDAY);
        rsDoctorRepository.saveDoctor("CT2", rsDoctorCT2);
        RSDoctor rsDoctorCT3 = new RSDoctor("RS C", "THT", "dr. Fizi",
                JADWAL31, JADWAL32, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        rsDoctorRepository.saveDoctor("CT3", rsDoctorCT3);
    }
}
