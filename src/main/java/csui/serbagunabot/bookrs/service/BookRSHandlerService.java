package csui.serbagunabot.bookrs.service;

import csui.serbagunabot.bookrs.core.RSUser;
import csui.serbagunabot.bookrs.repository.RSUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookRSHandlerService {

    @Autowired
    RSUserRepository rsUserRepository;

    Logger logger = LoggerFactory.getLogger("Reply Logging");

    /**
     * Handles message for bookrs logic.
     * @param  message message from the proxy server
     * @param userId as the sending user's id
     */
    public String handleMessage(String message, String userId) {
        String cleanMessage = message.replace("!doctor", "").trim();
        logger.info("Received bookrs message {} from {}", message, userId);

        RSUser rsUser = rsUserRepository
                .findById(userId);

        if (rsUser == null) {
            rsUser = rsUserRepository.createUserFromId(userId);
        }

        return rsUser.respond(cleanMessage);
    }
}
