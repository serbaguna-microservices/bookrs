package csui.serbagunabot.bookrs.controller;

import csui.serbagunabot.bookrs.model.line.LineRequest;
import csui.serbagunabot.bookrs.model.line.LineResponse;
import csui.serbagunabot.bookrs.service.BookRSHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookRSController {

    @Autowired
    BookRSHandlerService bookRSHandlerService;

    /**
     * Called by the proxy server if bookrs message was received.
     * @param lineRequest Instance containing message and userId
     */
    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<LineResponse> replyEntrypoint(@RequestBody LineRequest lineRequest) {
        String res = bookRSHandlerService
                .handleMessage(lineRequest.getMessage(), lineRequest.getUserId());

        return ResponseEntity.ok(new LineResponse(res));
    }
}
