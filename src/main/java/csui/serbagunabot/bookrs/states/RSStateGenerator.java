package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;

public class RSStateGenerator {
    private RSStateGenerator() {}

    /**
     * Generates initial states of bookrs feature when user sends !doctor.
     * @param rsUser rsUser linked to the user
     * @return the initial state of the chain of states
     */
    public static RSState initializingRSState(RSUser rsUser) {
        RSInitialState rsInitialState = new RSInitialState(rsUser);
        AskRSState askRSState = new AskRSState(rsUser);
        rsInitialState.setNextState(askRSState);

        ConfirmRSState confirmRSState = new ConfirmRSState(rsUser);
        askRSState.setNextState(confirmRSState);
        askRSState.setInitState(rsInitialState);

        AskDepartmentState askDeptState = new AskDepartmentState(rsUser);
        confirmRSState.setNextState(askDeptState);
        confirmRSState.setPrevState(askRSState);
        confirmRSState.setInitState(rsInitialState);

        ConfirmDeptState confirmDeptState = new ConfirmDeptState(rsUser);
        askDeptState.setNextState(confirmDeptState);
        askDeptState.setInitState(rsInitialState);

        AskNameState askNameState = new AskNameState(rsUser);
        confirmDeptState.setNextState(askNameState);
        confirmDeptState.setPrevState(askDeptState);
        confirmDeptState.setInitState(rsInitialState);

        ConfirmNameState confNameState = new ConfirmNameState(rsUser);
        askNameState.setNextState(confNameState);
        askNameState.setInitState(rsInitialState);

        AskDoctorState askDoctorState = new AskDoctorState(rsUser);
        confNameState.setNextState(askDoctorState);
        confNameState.setPrevState(askNameState);
        confNameState.setInitState(rsInitialState);

        AskAppointmentDateState askAppointmentDateState = new AskAppointmentDateState(rsUser);
        askDoctorState.setNextState(askAppointmentDateState);
        askDoctorState.setInitState(rsInitialState);

        ConfirmDoctorState confirmDoctorState = new ConfirmDoctorState(rsUser);
        askAppointmentDateState.setNextState(confirmDoctorState);
        askAppointmentDateState.setInitState(rsInitialState);
        askAppointmentDateState.setDoctorState(askDoctorState);

        confirmDoctorState.setPrevState(askDoctorState);
        confirmDoctorState.setInitState(rsInitialState);
        return rsInitialState;
    }
}
