package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;

public class ConfirmNameState implements RSState {
    private RSUser rsUser;

    private static final String NOT_FOUND_MSG = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG = "Apakah nama pasien yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap mengonfirmasi Nama Pasien%n%n"
            + "Nama Pasien: %s%n%n";

    RSState nextState;
    RSState prevState;
    RSState initState;

    public ConfirmNameState(RSUser rsUser) {
        this.rsUser = rsUser;
    }

    @Override
    public String getHandleMessage(String message) {
        String msg = message.toLowerCase();
        if (msg.equals("ya")) {
            rsUser.setCurrentState(nextState);
            String confNamaDone = String.format("[TERKONFIRMASI] Nama Pasien: %s %n%n",
                    rsUser.getNamaPasien());
            return confNamaDone + nextState.getOpeningMessage();
        } else if (msg.equals("tidak")) {
            rsUser.setCurrentState(prevState);
            return "Mengganti nama pasien\n\n" + prevState.getOpeningMessage();
        } else if (msg.equals("help")) {
            return String.format(HELP_MSG, rsUser.getNamaPasien()) + OPENING_MSG;
        }
        rsUser.getRsDoctor().setKode("");
        rsUser.setCurrentState(initState);
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public RSState getNextState() {
        return nextState;
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }

    public void setPrevState(RSState prevState) {
        this.prevState = prevState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }

    public RSState getPrevState() {
        return prevState;
    }
}