package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;

import java.time.LocalDate;

public class ConfirmDoctorState implements RSState {
    private RSUser rsUser;
    private RSDoctor rsDoctor;

    private static final String NOT_FOUND_MSG = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG = "Apakah pemesanan yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\nJika memilih Tidak "
            + "maka akan diarahkan untuk memilih dokter pada RS dan departemen yang sama.\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap mengonfirmasi booking RS%n%n"
            + "Dokter: %s Jadwal: %s, %s Jam: %s%n%n";

    private static final String FINAL_MESSAGE = "Terima kasih telah menggunakan layanan BookRS! :-)";

    RSState prevState;
    RSState initState;

    public ConfirmDoctorState(RSUser rsUser) {
        this.rsUser = rsUser;
        this.rsDoctor = this.rsUser.getRsDoctor();
    }

    @Override
    public String getHandleMessage(String message) {
        String msg = message.toLowerCase();
        if (msg.equals("ya")) {
            rsUser.setCurrentState(initState);
            String rs = rsUser.getRumahSakit();
            String dept = rsUser.getDepartemen();
            String confDoctorDone =
                    String.format("[TERKONFIRMASI] Anda menjadwalkan appointment di %s ", rs)
                    + String.format("pada Departemen %s ", dept);
            String confDoctorDone1 = String.format("bersama %s atas nama pasien: %s%n",
                    rsUser.getNamaDokter(), rsUser.getNamaPasien());
            rsDoctor = this.rsUser.getRsDoctor();
            String apptDay = rsDoctor.getApptDay();
            LocalDate apptDate = rsDoctor.getApptDate();
            String jam = rsDoctor.getApptClock();
            String confDoctorDone2 = String.format("Hari, Tanggal: %s, %s Jam: %s%n%n",
                    apptDay, apptDate, jam);
            return confDoctorDone + confDoctorDone1 + confDoctorDone2 + FINAL_MESSAGE;
        } else if (msg.equals("tidak")) {
            rsUser.setCurrentState(prevState);
            return "Mengganti dokter dan jadwalnya\n\n" + prevState.getOpeningMessage();
        } else if (msg.equals("help")) {
            rsDoctor = this.rsUser.getRsDoctor();
            String help = String.format(HELP_MSG, rsDoctor.getNama(), rsDoctor.getApptDay(),
                    rsDoctor.getApptDate(), rsDoctor.getApptClock());
            return help + OPENING_MSG;
        }
        rsUser.getRsDoctor().setKode("");
        rsUser.setCurrentState(initState);
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public void setPrevState(RSState prevState) {
        this.prevState = prevState;
    }

    public RSState getPrevState() {
        return prevState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }

    public RSState getInitState() {
        return initState;
    }
}
