package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;

public class AskRSState implements RSState {

    private RSDoctor rsDoctor;
    private RSUser rsUser;

    private static final String NOT_FOUND_MSG = "Nama RS tidak ditemukan!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG =
            "Silahkan pilih Rumah Sakit yang diinginkan dengan cara mengetik:\n"
                    + " '!doctor [Nomor RS]'(Contoh: !doctor 1)\n\n"
                    + "List RS yang tersedia:\n"
                    + "1. RS A\n2. RS B\n3. RS C\n\n"
                    + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap memilih Rumah Sakit\n\n";


    RSState nextState;
    RSState initState;

    public AskRSState(RSUser rsUser) {
        this.rsUser = rsUser;
        this.rsDoctor = this.rsUser.getRsDoctor();
    }

    @Override
    public String getHandleMessage(String msg) {
        String message = msg.toLowerCase();
        if (msg.equals("1")) {
            rsUser.setRumahSakit("RS A");
        } else if (msg.equals("2")) {
            rsUser.setRumahSakit("RS B");
        } else if (msg.equals("3")) {
            rsUser.setRumahSakit("RS C");
        } else if (message.equals("help")) {
            return HELP_MSG + OPENING_MSG;
        } else {
            rsDoctor.setKode("");
            rsUser.setCurrentState(initState);
            return NOT_FOUND_MSG;
        }
        String askRSDone = String.format("Anda memilih %s %n%n", rsUser.getRumahSakit());
        rsUser.setCurrentState(nextState);
        return askRSDone + nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public RSState getNextState() {
        return nextState;
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }
}

