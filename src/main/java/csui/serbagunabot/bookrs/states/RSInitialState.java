package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;

public class RSInitialState implements RSState {

    private RSUser rsUser;

    private static final String NOT_FOUND_MSG = "Perintah tidak ditemukan\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    RSState nextState;

    public RSInitialState(RSUser rsUser) {
        this.rsUser = rsUser;
    }

    @Override
    public String getHandleMessage(String message) {
        if (message.equals("")) {
            rsUser.setCurrentState(nextState);
            return getOpeningMessage() + nextState.getOpeningMessage();
        } else if (message.equals("help")) {
            return "Jika Anda membutuhkan bantuan untuk mengetahui state dimana "
                    + "Anda berada\nMaka ketik: !doctor help";
        }
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return "Selamat datang di fitur Booking RS!\n\n";
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }
}