package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class AskAppointmentDateState implements RSState {
    private RSUser rsUser;
    private RSDoctor rsDoctor;

    RSState nextState;
    RSState initState;
    RSState doctorState;

    private static final String OPENING_MSG =
            "Silahkan pilih jadwal %s yang "
                    + "diinginkan dengan cara mengetik:%n"
                    + " '!doctor [Nomor Jadwal]'(Contoh: !doctor 1)%n%n"
                    + "List Jadwal yang tersedia:%n"
                    + "1. %s %n2. %s %n3. Memilih dokter lain%n%n"
                    + "Jika membutuhkan bantuan maka ketik:%n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap memilih jadwal\n\n";

    private static final String NOT_FOUND_MSG = "Jadwal tidak ditemukan!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String CHECK_ANOTHER_DOCTOR = "Melihat jadwal dokter lain "
            + "di %s Departemen %s %n%n";

    public AskAppointmentDateState(RSUser rsUser) {
        this.rsUser = rsUser;
    }


    @Override
    public String getHandleMessage(String msg) {
        rsDoctor = rsUser.getRsDoctor();
        DayOfWeek hari;
        String hari1;
        String jam;
        if (msg.equals("1")) {
            String[] jdwl1split = rsDoctor.getJadwal1().split(" ");
            hari = rsDoctor.getHari1();
            hari1 = jdwl1split[0];
            jam = jdwl1split[1];
        } else if (msg.equals("2")) {
            String[] jdwl2split = rsDoctor.getJadwal2().split(" ");
            hari = rsDoctor.getHari2();
            hari1 = jdwl2split[0];
            jam = jdwl2split[1];
        } else if (msg.equals("3")) {
            String kode = rsUser.getRsDoctor().getKode();
            rsUser.setCurrentState(doctorState);
            rsUser.getRsDoctor().setKode(kode);
            String anotherDoctor = String.format(CHECK_ANOTHER_DOCTOR, rsUser.getRumahSakit(),
                    rsUser.getDepartemen());
            return anotherDoctor + doctorState.getOpeningMessage();
        } else if (msg.equalsIgnoreCase("help")) {
            return HELP_MSG + getOpeningMessage();
        } else {
            rsDoctor.setKode("");
            rsUser.setCurrentState(initState);
            return NOT_FOUND_MSG;
        }
        rsUser.setCurrentState(nextState);
        LocalDate ld = LocalDate.now();
        LocalDate apptDate = ld.with(TemporalAdjusters.next(hari));
        rsDoctor.setApptDate(apptDate, hari1, jam);
        String rs = rsUser.getRumahSakit();
        String dept = rsUser.getDepartemen();
        String askAptDateDone = String.format("Anda menjadwalkan appointment di %s ", rs)
                + String.format("pada Departemen %s ", dept)
                + String.format("bersama %s atas nama pasien: %s%n",
                rsUser.getNamaDokter(), rsUser.getNamaPasien())
                + String.format("Hari, Tanggal: %s, %s Jam: %s%n%n", hari1, apptDate, jam);
        return askAptDateDone + nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        rsDoctor = rsUser.getRsDoctor();
        return String.format(OPENING_MSG, rsDoctor.getNama(),
                rsDoctor.getJadwal1(), rsDoctor.getJadwal2());
    }

    public RSState getNextState() {
        return nextState;
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }

    public RSState getInitState() {
        return initState;
    }

    public RSState getDoctorState() {
        return doctorState;
    }

    public void setDoctorState(RSState doctorState) {
        this.doctorState = doctorState;
    }
}