package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;

public class AskDepartmentState implements RSState {
    private RSUser rsUser;
    private RSDoctor rsDoctor;

    RSState nextState;
    RSState initState;

    private static final String OPENING_MSG =
            "Silahkan pilih Departemen dokter yang "
                    + "diinginkan dengan cara mengetik:\n"
                    + " '!doctor [Nomor Departemen]'(Contoh: !doctor 1)\n\n"
                    + "List Departemen yang tersedia:\n"
                    + "1. Umum\n2. Gigi dan Bedah Mulut\n3. THT\n\n"
                    + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap menanyakan Departemen\n\n";

    private static final String NOT_FOUND_MSG = "Nama Departemen tidak ditemukan!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    public AskDepartmentState(RSUser rsUser) {
        this.rsUser = rsUser;
        this.rsDoctor = this.rsUser.getRsDoctor();
    }

    @Override
    public String getHandleMessage(String msg) {
        if (msg.equals("1")) {
            rsUser.setDepartemen("Umum");
        } else if (msg.equals("2")) {
            rsUser.setDepartemen("Gigi dan Bedah Mulut");
        } else if (msg.equals("3")) {
            rsUser.setDepartemen("THT");
        } else if (msg.equalsIgnoreCase("help")) {
            return HELP_MSG + OPENING_MSG;
        } else {
            rsDoctor.setKode("");
            rsUser.setCurrentState(initState);
            return NOT_FOUND_MSG;
        }
        String askDeptDone = String.format("Anda memilih Departemen %s di %s%n%n",
                rsUser.getDepartemen(), rsUser.getRumahSakit());
        rsUser.setCurrentState(nextState);
        return askDeptDone + nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public RSState getNextState() {
        return nextState;
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }
}
