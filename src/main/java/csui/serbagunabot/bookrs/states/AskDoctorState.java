package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.BookRSInitializer;
import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import csui.serbagunabot.bookrs.repository.RSDoctorRepository;
import csui.serbagunabot.bookrs.repository.RSDoctorRepositoryImpl;

public class AskDoctorState implements RSState {

    private RSUser rsUser;
    private RSDoctor rsDoctor;
    private BookRSInitializer bookRSInitializer;
    private RSDoctorRepository rsDoctorRepository;

    private static final String NOT_FOUND_MSG = "Dokter tidak ditemukan!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG = "Silahkan pilih dokter yang "
            + "diinginkan dengan cara mengetik:\n"
            + "'!doctor [Nomor Dokter]'(Contoh: !doctor 1)\n\n"
            + "List Dokter yang tersedia:\n";

    private static final String HELP_MSG = "Anda dalam tahap memilih dokter\n\n";

    RSState nextState;
    RSState initState;

    /**
     * Main constructor for AskDoctorState.
     * @param rsUser indicating that this state belongs to this rsUser
     */
    public AskDoctorState(RSUser rsUser) {
        this.bookRSInitializer = new BookRSInitializer();
        bookRSInitializer.init();
        this.rsDoctorRepository = RSDoctorRepositoryImpl.getDoctorRepository();
        this.rsUser = rsUser;
        this.rsDoctor = this.rsUser.getRsDoctor();
    }

    @Override
    public String getHandleMessage(String msg) {
        String cekKode = rsDoctor.getKode();
        int lenKode = cekKode.length();
        if (lenKode > 2) {
            String resetKode = resetKode(cekKode);
            rsDoctor.setKode(resetKode);
        }

        if (msg.equals("1")) {
            rsDoctor.makeKode("1");
        } else if (msg.equals("2")) {
            rsDoctor.makeKode("2");
        } else if (msg.equals("3")) {
            rsDoctor.makeKode("3");
        } else if (msg.equalsIgnoreCase("help")) {
            return HELP_MSG + getOpeningMessage();
        } else {
            rsDoctor.setKode("");
            rsUser.setCurrentState(initState);
            return NOT_FOUND_MSG;
        }
        String kode = rsDoctor.getKode();
        RSDoctor rsDoctor1 = rsDoctorRepository.findByKode(kode);
        rsUser.setRsDoctor(rsDoctor1);
        rsUser.setNamaDokter(rsDoctor1.getNama());
        String rs = rsUser.getRumahSakit();
        String dept = rsUser.getDepartemen();
        String askDoctorDone = String.format("Anda memilih %s pada Departemen %s ", rs, dept)
                + String.format("bersama %s atas nama pasien: %s%n%n",
                rsUser.getNamaDokter(), rsUser.getNamaPasien());
        rsUser.setCurrentState(nextState);
        return askDoctorDone + nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        String lstDokter;
        if (rsUser.getRumahSakit().equals("RS A")) {
            if (rsUser.getDepartemen().equals("Umum")) {
                lstDokter = "1. dr. Astuti\n2. dr. Budiman\n3. dr. Carita";
            } else if (rsUser.getDepartemen().equals("THT")) {
                lstDokter = "1. dr. Diandra\n2. dr. Edison\n3. dr. Fumala";
            } else {
                lstDokter = "1. drg. Gilang\n2. drg. Heni\n3. drg. Indah";
            }
        } else if (rsUser.getRumahSakit().equals("RS B")) {
            if (rsUser.getDepartemen().equals("Umum")) {
                lstDokter = "1. dr. Alika\n2. dr. Bertha\n3. dr. Charlie";
            } else if (rsUser.getDepartemen().equals("THT")) {
                lstDokter = "1. dr. Dilan\n2. dr. Endah\n3. dr. Fitri";
            } else {
                lstDokter = "1. drg. Gunawan\n2. drg. Hani\n3. drg. Imara";
            }
        } else {
            if (rsUser.getDepartemen().equals("Umum")) {
                lstDokter = "1. dr. Amanda\n2. dr. Bambang\n3. dr. Carla";
            } else if (rsUser.getDepartemen().equals("THT")) {
                lstDokter = "1. dr. Doni\n2. dr. Emma\n3. dr. Fizi";
            } else {
                lstDokter = "1. drg. Gani\n2. drg. Hilman\n3. drg. Indra";
            }
        }
        String help = "\n\nJika membutuhkan bantuan maka ketik:\n'!doctor help'";
        return OPENING_MSG + lstDokter + help;
    }

    /**
     * Method for resetting code in RSDoctor.
     * @param kode the code that will be changed
     * @return new code
     */
    public String resetKode(String kode) {
        int lenKode = kode.length();
        if (lenKode == 3) {
            return kode.substring(0,2);
        } else {
            return kode.substring(lenKode - 2, lenKode);
        }
    }

    public RSState getNextState() {
        return nextState;
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }

    public RSState getInitState() {
        return initState;
    }
}
