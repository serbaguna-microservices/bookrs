package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConfirmDeptState implements RSState {
    RSUser rsUser;
    RSDoctor rsDoctor;

    private static final String NOT_FOUND_MSG = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG = "Apakah Departemen yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap mengonfirmasi Departemen%n%n"
            + "Departemen: %s %n%n";

    RSState nextState;
    RSState prevState;
    RSState initState;

    public ConfirmDeptState(RSUser rsUser) {
        this.rsUser = rsUser;
        this.rsDoctor = this.rsUser.getRsDoctor();
    }

    @Override
    public String getHandleMessage(String message) {
        String msg = message.toLowerCase();
        String dept = rsUser.getDepartemen();
        if (msg.equals("ya")) {
            makeKode(dept);
            rsUser.setCurrentState(nextState);
            String confDeptDone = String.format("[TERKONFIRMASI] Departemen: %s %n%n",
                    rsUser.getDepartemen());
            return confDeptDone + nextState.getOpeningMessage();
        } else if (msg.equals("tidak")) {
            rsUser.setCurrentState(prevState);
            return "Mengganti departemen\n\n" + prevState.getOpeningMessage();
        } else if (msg.equals("help")) {
            return String.format(HELP_MSG, rsUser.getDepartemen()) + OPENING_MSG;
        }
        rsUser.getRsDoctor().setKode("");
        rsUser.setCurrentState(initState);
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public void makeKode(String dept) {
        if (dept.equals("Umum")) {
            rsDoctor.makeKode("U");
        } else if (dept.equals("THT")) {
            rsDoctor.makeKode("T");
        } else {
            rsDoctor.makeKode("G");
        }
    }
}
