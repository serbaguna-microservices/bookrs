package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConfirmRSState implements RSState {
    private RSUser rsUser;
    private RSDoctor rsDoctor;

    private static final String NOT_FOUND_MSG = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG = "Apakah RS yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap mengonfirmasi Rumah Sakit%n%n"
            + "Rumah Sakit: %s %n%n";

    RSState nextState;
    RSState prevState;
    RSState initState;

    public ConfirmRSState(RSUser rsUser) {
        this.rsUser = rsUser;
        this.rsDoctor = this.rsUser.getRsDoctor();
    }

    @Override
    public String getHandleMessage(String message) {
        String msg = message.toLowerCase();
        String rs = rsUser.getRumahSakit();
        if (msg.equals("ya")) {
            makeKode(rs);
            rsUser.setCurrentState(nextState);
            String confRSDone = String.format("[TERKONFIRMASI] Rumah Sakit: %s %n%n",
                    rsUser.getRumahSakit());
            return confRSDone + nextState.getOpeningMessage();
        } else if (msg.equals("tidak")) {
            rsUser.setCurrentState(prevState);
            return "Mengganti rumah sakit\n\n" + prevState.getOpeningMessage();
        } else if (msg.equals("help")) {
            return String.format(HELP_MSG, rsUser.getRumahSakit()) + OPENING_MSG;
        }
        rsUser.getRsDoctor().setKode("");
        rsUser.setCurrentState(initState);
        return NOT_FOUND_MSG;
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public void makeKode(String rs) {
        if (rs.equals("RS A")) {
            rsDoctor.makeKode("A");
        } else if (rs.equals("RS B")) {
            rsDoctor.makeKode("B");
        } else {
            rsDoctor.makeKode("C");
        }
    }
}
