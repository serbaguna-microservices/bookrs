package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;

public class AskNameState implements RSState {
    private RSUser rsUser;

    private static final String NOT_FOUND_MSG = "Tidak memasukkan nama pasien!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private static final String OPENING_MSG = "Silahkan masukkan Nama Pasien "
            + "dengan format:\n'!doctor [Nama Pasien]' "
            + "contoh: !doctor Budi Dubi\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private static final String HELP_MSG = "Anda dalam tahap mengisi Nama Pasien\n\n";

    private static final String INVALID_NAME_MSG = "Nama yang diberikan tidak valid!\n"
            + "Mohon memasukkan nama tanpa angka ataupun simbol!\n\n";

    RSState nextState;
    RSState initState;

    public AskNameState(RSUser rsUser) {
        this.rsUser = rsUser;
    }

    @Override
    public String getHandleMessage(String message) {
        if (message.equals("")) {
            rsUser.getRsDoctor().setKode("");
            rsUser.setCurrentState(initState);
            return NOT_FOUND_MSG;
        } else if (message.equalsIgnoreCase("help")) {
            return HELP_MSG + OPENING_MSG;
        }
        String msgLower = message.toLowerCase();
        char[] chars = msgLower.toCharArray();
        for (char ch: chars) {
            if (!(ch >= 'a' && ch <= 'z')) {
                if (ch == ' ') {
                    continue;
                }
                rsUser.setCurrentState(this);
                return INVALID_NAME_MSG + rsUser.getCurrentState().getOpeningMessage();
            }
        }
        rsUser.setNamaPasien(message);
        String askNamaDone = String.format("Nama Pasien: %s %n%n", rsUser.getNamaPasien());
        rsUser.setCurrentState(nextState);
        return askNamaDone + nextState.getOpeningMessage();
    }

    @Override
    public String getOpeningMessage() {
        return OPENING_MSG;
    }

    public RSState getNextState() {
        return nextState;
    }

    public void setNextState(RSState nextState) {
        this.nextState = nextState;
    }

    public void setInitState(RSState initState) {
        this.initState = initState;
    }
}

