package csui.serbagunabot.bookrs.states;

public interface RSState {
    public String getHandleMessage(String message);

    public String getOpeningMessage();
}
