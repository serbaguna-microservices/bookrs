package csui.serbagunabot.bookrs;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.repository.RSDoctorRepository;
import csui.serbagunabot.bookrs.repository.RSDoctorRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

class BookRSInitializerTest {

    @InjectMocks
    private BookRSInitializer bookRSInitializer;

    private RSDoctorRepository rsDoctorRepository;

    @BeforeEach
    void setUp() {
        bookRSInitializer = new BookRSInitializer();
        bookRSInitializer.init();
        rsDoctorRepository = RSDoctorRepositoryImpl.getDoctorRepository();
    }

    @Test
    void doctorInitializedTest() {
        RSDoctor doctor = rsDoctorRepository.findByKode("AU1");

        Assertions.assertEquals("dr. Astuti", doctor.getNama());
    }
}
