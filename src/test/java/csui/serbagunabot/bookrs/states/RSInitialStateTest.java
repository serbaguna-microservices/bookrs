package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RSInitialStateTest {

    private RSInitialState rsInitialState;
    private RSUser rsUser;

    /**
     * Sets up the rsUser and the next state.
     */
    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.rsInitialState = new RSInitialState(rsUser);
        rsInitialState.setNextState(new AskRSState(rsUser));
    }

    @Test
    void testRSInitialOpeningMsg() {
        String res = rsInitialState.getOpeningMessage();
        Assertions.assertEquals("Selamat datang di fitur Booking RS!\n\n", res);
    }

    @Test
    void testRSInitialStateNotFoundMsg() {
        String res = rsInitialState.getHandleMessage("TEST");
        Assertions.assertEquals("Perintah tidak ditemukan\n"
                + "Ketik !doctor untuk mulai menggunakan fitur Booking RS", res);
    }

    @Test
    void testRSInitialStateMoveToNextState() {
        String res = rsInitialState.getHandleMessage("");
        Assertions.assertEquals(res, "Selamat datang di fitur Booking RS!\n\n"
                + "Silahkan pilih Rumah Sakit yang diinginkan dengan cara mengetik:\n"
                + " '!doctor [Nomor RS]'(Contoh: !doctor 1)\n\nList RS yang tersedia:\n"
                + "1. RS A\n2. RS B\n3. RS C\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'");
        Assertions.assertNotEquals(rsInitialState, rsUser.getCurrentState());
    }

    @Test
    void handleMessageHelpTest() {
        String res = rsInitialState.getHandleMessage("help");

        Assertions.assertEquals("Jika Anda membutuhkan bantuan untuk mengetahui state dimana "
                + "Anda berada\nMaka ketik: !doctor help", res);
    }
}
