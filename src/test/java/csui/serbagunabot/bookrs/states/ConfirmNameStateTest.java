package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConfirmNameStateTest {
    RSUser rsUser;
    ConfirmNameState confNameState;

    private final String helpMsg = "Anda dalam tahap mengonfirmasi Nama Pasien%n%n"
            + "Nama Pasien: X%n%n";

    /**
     * Sets up the rsUser, the next state, and the init state.
     */
    @BeforeEach
    void setUp() {
        this.rsUser = new RSUser();
        this.confNameState = new ConfirmNameState(rsUser);
        confNameState.setNextState(new AskDoctorState(rsUser));
        confNameState.setPrevState(new AskNameState(rsUser));
        confNameState.setInitState(new RSInitialState(rsUser));
        rsUser.setNamaPasien("X");
        rsUser.setRumahSakit("RS A");
        rsUser.setDepartemen("Umum");
    }

    @Test
    void testOpeningMessage() {
        String res = confNameState.getOpeningMessage();

        Assertions.assertEquals("Apakah nama pasien yang tertera sudah benar?\n"
                + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
                + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
                + "Contoh: !doctor Ya\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }


    @Test
    void testHandleMessageNotFound() {
        String res = confNameState.getHandleMessage("test");
        Assertions.assertEquals("Tidak memilih Ya/Tidak!\n"
                + "Ketik !doctor untuk mulai menggunakan fitur Booking RS", res);
        Assertions.assertNotEquals(rsUser.getCurrentState(), confNameState.getNextState());
    }

    @Test
    void testHandleMessageYa() {
        String res = confNameState.getHandleMessage("Ya");
        Assertions.assertEquals(String.format("[TERKONFIRMASI] Nama Pasien: X %n%n")
                + "Silahkan pilih dokter yang "
                + "diinginkan dengan cara mengetik:\n"
                + "'!doctor [Nomor Dokter]'(Contoh: !doctor 1)\n\n"
                + "List Dokter yang tersedia:\n"
                + "1. dr. Astuti\n2. dr. Budiman\n3. dr. Carita\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), confNameState.getNextState());

    }

    @Test
    void testHandleMessageTidak() {
        String res = confNameState.getHandleMessage("tidak");
        Assertions.assertEquals("Mengganti nama pasien\n\n"
                + "Silahkan masukkan Nama Pasien "
                + "dengan format:\n'!doctor [Nama Pasien]' "
                + "contoh: !doctor Budi Dubi\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), confNameState.getPrevState());
    }

    @Test
    void testHandleMessageHelp() {
        String res = confNameState.getHandleMessage("help");

        Assertions.assertEquals(String.format(helpMsg)
                + "Apakah nama pasien yang tertera sudah benar?\n"
                + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
                + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
                + "Contoh: !doctor Ya\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }
}
