package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

class ConfirmDeptStateTest {
    RSUser rsUser;
    ConfirmDeptState confirmDeptState;

    private final String openingMsg = "Apakah Departemen yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private final String notFoundMsg = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private final String helpMsg = "Anda dalam tahap mengonfirmasi Departemen%n%n"
            + "Departemen: %s %n%n";

    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.confirmDeptState = new ConfirmDeptState(rsUser);
        this.confirmDeptState.setNextState(new ConfirmDeptState(rsUser));
        this.confirmDeptState.setPrevState(new ConfirmDeptState(rsUser));
        rsUser.setDepartemen("Umum");
        rsUser.setRumahSakit("RS A");
    }

    @Test
    void getOpeningMessageTest() {
        String res = confirmDeptState.getOpeningMessage();

        Assertions.assertEquals(openingMsg, res);
    }

    @Test
    void getHandleMessageNotFoundTest() {
        String res = confirmDeptState.getHandleMessage("");

        Assertions.assertEquals(notFoundMsg, res);
    }

    @Test
    void getHandleMessageYaTest() {
        String res = confirmDeptState.getHandleMessage("ya");
        String exp = String.format("[TERKONFIRMASI] Departemen: %s %n%n",
                rsUser.getDepartemen());
        Assertions.assertEquals(exp + openingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), confirmDeptState.getNextState());
    }

    @Test
    void getHandleMessageTidakTest() {
        String res = confirmDeptState.getHandleMessage("tidak");
        String exp = "Mengganti departemen\n\n";
        Assertions.assertEquals(exp + openingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), confirmDeptState.getPrevState());
    }

    @ParameterizedTest
    @CsvSource({"Umum, U", "THT, T", "Gigi dan Bedah Mulut, G"})
    void makeKodeTest(ArgumentsAccessor argumentsAccessor) {
        String rs = argumentsAccessor.getString(0);
        String kode = argumentsAccessor.getString(1);
        confirmDeptState.makeKode(rs);

        Assertions.assertEquals(kode, rsUser.getRsDoctor().getKode());
    }

    @Test
    void handleMsgHelpTest() {
        String res = confirmDeptState.getHandleMessage("help");

        Assertions.assertEquals(String.format(helpMsg, "Umum")
                + openingMsg, res);
    }

    @Test
    void setterGetterRSUser() {
        confirmDeptState.setRsUser(rsUser);

        Assertions.assertEquals(rsUser, confirmDeptState.getRsUser());
    }

    @Test
    void setterGetterRSDoctor() {
        confirmDeptState.setRsDoctor(new RSDoctor());
        Assertions.assertNotNull(confirmDeptState.getRsDoctor());
    }

    @Test
    void setterGetterInitState() {
        confirmDeptState.setInitState(new ConfirmRSState(rsUser));
        Assertions.assertNotNull(confirmDeptState.getInitState());
    }
}
