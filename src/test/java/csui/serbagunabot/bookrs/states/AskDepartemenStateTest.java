package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AskDepartemenStateTest {
    RSUser rsUser;
    AskDepartmentState askDeptState;

    /**
     * Sets up the rsUser, the next state, and the init state.
     */
    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.askDeptState = new AskDepartmentState(rsUser);
        askDeptState.setNextState(new AskNameState(rsUser));
        askDeptState.setInitState(new RSInitialState(rsUser));
        rsUser.setRumahSakit("X");
    }

    @Test
    void testHandleMsgUmum() {
        String res = askDeptState.getHandleMessage("1");

        Assertions.assertEquals("Umum", rsUser.getDepartemen());
        Assertions.assertEquals(String.format("Anda memilih Departemen Umum di X%n%n")
                + "Silahkan masukkan Nama Pasien "
                + "dengan format:\n'!doctor [Nama Pasien]' "
                + "contoh: !doctor Budi Dubi\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), askDeptState.getNextState());
    }

    @Test
    void testHandleMsgGigi() {
        String res = askDeptState.getHandleMessage("2");

        Assertions.assertEquals("Gigi dan Bedah Mulut", rsUser.getDepartemen());
        Assertions.assertEquals(String.format("Anda memilih Departemen Gigi dan "
                + "Bedah Mulut di X%n%n")
                + "Silahkan masukkan Nama Pasien "
                + "dengan format:\n'!doctor [Nama Pasien]' "
                + "contoh: !doctor Budi Dubi\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), askDeptState.getNextState());
    }

    @Test
    void testHandleMsgTHT() {
        String res = askDeptState.getHandleMessage("3");

        Assertions.assertEquals("THT", rsUser.getDepartemen());
        Assertions.assertEquals(String.format("Anda memilih Departemen THT di X%n%n")
                + "Silahkan masukkan Nama Pasien "
                + "dengan format:\n'!doctor [Nama Pasien]' "
                + "contoh: !doctor Budi Dubi\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), askDeptState.getNextState());
    }

    @Test
    void testHandleMsgNotFound() {
        String res = askDeptState.getHandleMessage("TEST");

        Assertions.assertEquals("Nama Departemen tidak ditemukan!\n"
                + "Ketik !doctor untuk mulai menggunakan fitur Booking RS", res);
        Assertions.assertNotEquals(rsUser.getCurrentState(), askDeptState.getNextState());
    }

    @Test
    void testOpeningMSg() {
        String res = askDeptState.getOpeningMessage();

        Assertions.assertEquals("Silahkan pilih Departemen dokter yang "
                + "diinginkan dengan cara mengetik:\n"
                + " '!doctor [Nomor Departemen]'(Contoh: !doctor 1)\n\n"
                + "List Departemen yang tersedia:\n"
                + "1. Umum\n2. Gigi dan Bedah Mulut\n3. THT\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }

    @Test
    void getHandleMessageHelpTest() {
        String res = askDeptState.getHandleMessage("help");

        Assertions.assertEquals("Anda dalam tahap menanyakan Departemen\n\n"
                + "Silahkan pilih Departemen dokter yang "
                + "diinginkan dengan cara mengetik:\n"
                + " '!doctor [Nomor Departemen]'(Contoh: !doctor 1)\n\n"
                + "List Departemen yang tersedia:\n"
                + "1. Umum\n2. Gigi dan Bedah Mulut\n3. THT\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }
}
