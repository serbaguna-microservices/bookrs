package csui.serbagunabot.bookrs.states;


import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

class AskDoctorStateTest {
    RSUser rsUser;
    RSDoctor rsDoctor;
    AskDoctorState askDoctorState;

    private final String openingMsg = "Silahkan pilih dokter yang diinginkan "
            + "dengan cara mengetik:\n'!doctor [Nomor Dokter]'(Contoh: !doctor 1)\n\n"
            + "List Dokter yang tersedia:\n";

    private final String open1 =
            "Silahkan pilih jadwal %s yang "
                    + "diinginkan dengan cara mengetik:%n"
                    + " '!doctor [Nomor Jadwal]'(Contoh: !doctor 1)%n%n"
                    + "List Jadwal yang tersedia:%n"
                    + "1. %s %n2. %s %n3. Memilih dokter lain%n%n"
                    + "Jika membutuhkan bantuan maka ketik:%n'!doctor help'";

    private final String handleMsg = "Anda memilih %s pada Departemen %s "
            + "bersama %s atas nama pasien: X%n%n";

    private final String notFoundMsg = "Dokter tidak ditemukan!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    /**
     * Sets up the rsUser, the next state, and the init state.
     */
    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.rsDoctor = this.rsUser.getRsDoctor();
        this.askDoctorState = new AskDoctorState(rsUser);
        askDoctorState.setNextState(new AskAppointmentDateState(rsUser));
        askDoctorState.setInitState(new RSInitialState(rsUser));
        rsUser.setNamaPasien("X");
    }

    @ParameterizedTest
    @CsvSource({"RS A, THT, 1. dr. Diandra, 2. dr. Edison, 3. dr. Fumala",
            "RS A, Gigi dan Bedah Mulut, 1. drg. Gilang, 2. drg. Heni, 3. drg. Indah",
            "RS B, Umum, 1. dr. Alika, 2. dr. Bertha, 3. dr. Charlie",
            "RS B, THT, 1. dr. Dilan, 2. dr. Endah, 3. dr. Fitri",
            "RS B, Gigi dan Bedah Mulut, 1. drg. Gunawan, 2. drg. Hani, 3. drg. Imara",
            "RS C, Umum, 1. dr. Amanda, 2. dr. Bambang, 3. dr. Carla",
            "RS C, THT, 1. dr. Doni, 2. dr. Emma, 3. dr. Fizi",
            "RS C, Gigi dan Bedah Mulut, 1. drg. Gani, 2. drg. Hilman, 3. drg. Indra"})
    void testGetOpeningMsg(ArgumentsAccessor argumentsAccessor) {
        String rs = argumentsAccessor.getString(0);
        String dept = argumentsAccessor.getString(1);
        String expOpening1 = argumentsAccessor.getString(2);
        String expOpening2 = argumentsAccessor.getString(3);
        String expOpening3 = argumentsAccessor.getString(4);

        String expOpening = expOpening1 + "\n" + expOpening2 + "\n" + expOpening3 + "\n\n";
        rsUser.setRumahSakit(rs);
        rsUser.setDepartemen(dept);
        String res = askDoctorState.getOpeningMessage();

        Assertions.assertEquals(openingMsg + expOpening
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }

    @ParameterizedTest
    @CsvSource({"RS A, Umum, AU, 1", "RS A, Umum, AU, 2", "RS A, Umum, AU, 3",
            "RS A, Gigi dan Bedah Mulut, AG, 1", "RS A, Gigi dan Bedah Mulut, AG, 2",
            "RS A, Gigi dan Bedah Mulut, AG, 3", "RS A, THT, AT, 1",
            "RS A, THT, AT, 2", "RS A, THT, AT, 3",
            "RS B, Umum, BU, 1", "RS B, Umum, BU, 2", "RS B, Umum, BU, 3",
            "RS B, Gigi dan Bedah Mulut, BG, 1", "RS B, Gigi dan Bedah Mulut, BG, 2",
            "RS B, Gigi dan Bedah Mulut, BG, 3", "RS B, THT, BT, 1",
            "RS B, THT, BT, 2", "RS B, THT, BT, 3",
            "RS C, Umum, CU, 1", "RS C, Umum, CU, 2", "RS C, Umum, CU, 3",
            "RS C, Gigi dan Bedah Mulut, CG, 1", "RS C, Gigi dan Bedah Mulut, CG, 2",
            "RS C, Gigi dan Bedah Mulut, CG, 3", "RS C, THT, CT, 1",
            "RS C, THT, CT, 2", "RS C, THT, CT, 3"})
    void testGetHandleMsgFound(ArgumentsAccessor argumentsAccessor) {
        String rs = argumentsAccessor.getString(0);
        String dept = argumentsAccessor.getString(1);
        String kode = argumentsAccessor.getString(2);

        rsUser.setRumahSakit(rs);
        rsUser.setDepartemen(dept);
        rsDoctor.setKode(kode);

        String msg = argumentsAccessor.getString(3);
        String res = askDoctorState.getHandleMessage(msg);
        RSDoctor rsDoctor1 = rsUser.getRsDoctor();
        String openingNext = String.format(open1, rsUser.getNamaDokter(),
                rsDoctor1.getJadwal1(), rsDoctor1.getJadwal2());
        Assertions.assertEquals(String.format(handleMsg + openingNext, rs,
                dept, rsUser.getNamaDokter()), res);
        Assertions.assertEquals(rsUser.getCurrentState(), askDoctorState.getNextState());
    }

    @Test
    void testHandleMsgResetKode() {
        rsUser.getRsDoctor().setKode("AU3");

        String res = askDoctorState.getHandleMessage("2");
        RSDoctor rsDoctor1 = rsUser.getRsDoctor();
        String openingNext = String.format(open1, rsUser.getNamaDokter(),
                rsDoctor1.getJadwal1(), rsDoctor1.getJadwal2());
        Assertions.assertEquals(String.format(handleMsg + openingNext, null,
                null, rsUser.getNamaDokter()), res);
    }

    @Test
    void testHandleMsgNotFound() {
        rsUser.setDepartemen("Umum");
        rsUser.setRumahSakit("RS A");

        String res = askDoctorState.getHandleMessage("4");

        Assertions.assertEquals(notFoundMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), askDoctorState.getInitState());
    }

    @Test
    void testResetKode() {
        String res = askDoctorState.resetKode("AT3BU");

        Assertions. assertEquals("BU", res);
    }

    @Test
    void testGetHandleMsgHelp() {
        rsUser.setRumahSakit("RS A");
        rsUser.setDepartemen("Umum");
        String res = askDoctorState.getHandleMessage("help");

        Assertions.assertEquals("Anda dalam tahap memilih dokter\n\n"
                + askDoctorState.getOpeningMessage(), res);
    }
}
