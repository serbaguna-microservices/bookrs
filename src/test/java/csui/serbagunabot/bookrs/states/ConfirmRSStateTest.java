package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

class ConfirmRSStateTest {
    RSUser rsUser;
    ConfirmRSState confirmRSState;

    private final String openingMsg = "Apakah RS yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";;

    private final String notFoundMsg = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private final String helpMsg = "Anda dalam tahap mengonfirmasi Rumah Sakit%n%n"
            + "Rumah Sakit: RS A %n%n";

    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.confirmRSState = new ConfirmRSState(rsUser);
        this.confirmRSState.setNextState(new ConfirmRSState(rsUser));
        this.confirmRSState.setPrevState(new ConfirmRSState(rsUser));
        this.confirmRSState.setInitState(new RSInitialState(rsUser));
        rsUser.setRumahSakit("RS A");
    }

    @Test
    void getOpeningMessageTest() {
        String res = confirmRSState.getOpeningMessage();

        Assertions.assertEquals(openingMsg, res);
    }

    @Test
    void getHandleMessageNotFoundTest() {
        String res = confirmRSState.getHandleMessage("");

        Assertions.assertEquals(notFoundMsg, res);
    }

    @Test
    void getHandleMsgYaTest() {
        String res = confirmRSState.getHandleMessage("ya");
        String exp = String.format("[TERKONFIRMASI] Rumah Sakit: %s %n%n",
                rsUser.getRumahSakit());
        Assertions.assertEquals(exp + openingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), confirmRSState.getNextState());
    }

    @Test
    void getHandleMsgTidak() {
        String res = confirmRSState.getHandleMessage("tidak");
        String exp = "Mengganti rumah sakit\n\n";
        Assertions.assertEquals(exp + openingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), confirmRSState.getPrevState());
    }

    @ParameterizedTest
    @CsvSource({"RS A, A", "RS B, B", "RS C, C"})
    void makeKodeTest(ArgumentsAccessor argumentsAccessor) {
        String rs = argumentsAccessor.getString(0);
        String kode = argumentsAccessor.getString(1);
        confirmRSState.makeKode(rs);

        Assertions.assertEquals(kode, rsUser.getRsDoctor().getKode());
    }

    @Test
    void handleMessageHelpTest() {
        String res = confirmRSState.getHandleMessage("help");

        Assertions.assertEquals(String.format(helpMsg)
                + openingMsg, res);
    }

    @Test
    void setterGetterRSUser() {
        confirmRSState.setRsUser(rsUser);
        Assertions.assertEquals(rsUser, confirmRSState.getRsUser());
    }

    @Test
    void setterGetterRSDoctor() {
        confirmRSState.setRsDoctor(new RSDoctor());
        Assertions.assertNotNull(confirmRSState.getRsDoctor());
    }

    @Test
    void setterGetterInitState() {
        confirmRSState.setInitState(new ConfirmRSState(rsUser));
        Assertions.assertNotNull(confirmRSState.getInitState());
    }
}
