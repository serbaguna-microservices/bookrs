package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSDoctor;
import csui.serbagunabot.bookrs.core.RSUser;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

class AskAppointmentDateStateTest {
    RSUser rsUser;
    RSDoctor rsDoctor;
    AskAppointmentDateState askAppointmentDateState;
    AskDoctorState askDoctorState;

    private final String openingMsg = String.format("Silahkan pilih jadwal drg. Indah yang "
            + "diinginkan dengan cara mengetik:%n"
            + " '!doctor [Nomor Jadwal]'(Contoh: !doctor 1)%n%n"
            + "List Jadwal yang tersedia:%n"
            + "1. Jumat 08.00-11.00 %n2. Sabtu 13.00-16.00 %n3. Memilih dokter lain%n%n"
            + "Jika membutuhkan bantuan maka ketik:%n'!doctor help'");

    private final String tempEnd = "Terima kasih telah menggunakan layanan BookRS! :-)";

    private final String notFoundMsg = "Jadwal tidak ditemukan!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private final String handleMsg = "Anda menjadwalkan appointment di null "
            + "pada Departemen null "
            + "bersama null atas nama pasien: null%n"
            + "Hari, Tanggal: %s, %s Jam: %s%n%n";

    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.rsDoctor = new RSDoctor("RS C", "Gigi dan Bedah Mulut", "drg. Indah",
                "Jumat 08.00-11.00","Sabtu 13.00-16.00", DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
        this.rsUser.setRsDoctor(rsDoctor);
        this.askAppointmentDateState = new AskAppointmentDateState(rsUser);
        this.askDoctorState = new AskDoctorState(rsUser);
        askAppointmentDateState.setDoctorState(askDoctorState);
        askAppointmentDateState.setNextState(new AskAppointmentDateState(rsUser));
    }

    @Test
    void getOpeningMsgTest() {
        String res = askAppointmentDateState.getOpeningMessage();

        Assertions.assertEquals(openingMsg, res);
    }

    @ParameterizedTest
    @CsvSource({"1, FRIDAY, Jumat, 08.00-11.00",
            "2, SATURDAY, Sabtu, 13.00-16.00"})
    void getHandleMessageFound(ArgumentsAccessor argumentsAccessor) {
        String msg = argumentsAccessor.getString(0);
        String strDay = argumentsAccessor.getString(1);
        DayOfWeek day = DayOfWeek.valueOf(strDay);
        String hari = argumentsAccessor.getString(2);
        String jam = argumentsAccessor.getString(3);

        LocalDate date = LocalDate.now();
        LocalDate date1 = date.with(TemporalAdjusters.next(day));
        String formatHandle = String.format(handleMsg, hari, date1, jam);
        String res = askAppointmentDateState.getHandleMessage(msg);

        Assertions.assertEquals(formatHandle  + openingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), askAppointmentDateState.getNextState());
    }

    @Test
    void getHandleMessageAnotherDoctorTest() {
        this.rsUser.setRumahSakit("RS C");
        this.rsUser.setDepartemen("Umum");
        String res = askAppointmentDateState.getHandleMessage("3");
        String result1 = String.format("Melihat jadwal dokter lain di RS C Departemen Umum %n%n");
        Assertions.assertEquals(result1 + askDoctorState.getOpeningMessage(), res);
        Assertions.assertNotNull(askAppointmentDateState.getDoctorState());
    }

    @Test
    void getHandleMessageNotFound() {
        String res = askAppointmentDateState.getHandleMessage("4");

        Assertions.assertEquals(notFoundMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), askAppointmentDateState.getInitState());
    }

    @Test
    void getHandleMessageHelpTest() {
        String res = askAppointmentDateState.getHandleMessage("help");

        Assertions.assertEquals("Anda dalam tahap memilih jadwal\n\n"
                + openingMsg, res);
    }
}
