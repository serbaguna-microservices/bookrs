package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RSStateGeneratorTest {

    RSUser rsUser;

    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
    }

    @Test
    void initalizingRSStateTest() {
        RSState rsState = RSStateGenerator.initializingRSState(rsUser);

        Assertions.assertTrue(rsState instanceof RSInitialState);
    }
}
