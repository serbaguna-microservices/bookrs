package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConfirmDoctorStateTest {
    RSUser rsUser;
    ConfirmDoctorState confirmDoctorState;

    private final String openingMsg = "Apakah pemesanan yang tertera sudah benar?\n"
            + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
            + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
            + "Contoh: !doctor Ya\n\nJika memilih Tidak "
            + "maka akan diarahkan untuk memilih dokter pada RS dan departemen yang sama.\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private final String notFoundMsg = "Tidak memilih Ya/Tidak!\n"
            + "Ketik !doctor untuk mulai menggunakan fitur Booking RS";

    private final String helpMsg = "Anda dalam tahap mengonfirmasi booking RS%n%n"
            + "Dokter: null Jadwal: null, null Jam: null%n%n";

    private String endingMsg = "Terima kasih telah menggunakan layanan BookRS! :-)";

    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.confirmDoctorState = new ConfirmDoctorState(rsUser);
        this.confirmDoctorState.setInitState(new ConfirmDoctorState(rsUser));
        this.confirmDoctorState.setPrevState(new ConfirmDoctorState(rsUser));
        rsUser.setDepartemen("Umum");
        rsUser.setRumahSakit("RS A");
    }

    @Test
    void getOpeningMessageTest() {
        String res = confirmDoctorState.getOpeningMessage();

        Assertions.assertEquals(openingMsg, res);
    }

    @Test
    void getHandleMessageNotFoundTest() {
        String res = confirmDoctorState.getHandleMessage("");

        Assertions.assertEquals(notFoundMsg, res);
    }

    @Test
    void getHandleMessageYaTest() {
        String res = confirmDoctorState.getHandleMessage("ya");
        String exp = String.format("[TERKONFIRMASI] Anda menjadwalkan appointment di RS A ")
                + String.format("pada Departemen Umum ")
                + String.format("bersama null atas nama pasien: null%n")
                + String.format("Hari, Tanggal: null, null Jam: null%n%n");
        Assertions.assertEquals(exp + endingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), confirmDoctorState.getInitState());
    }

    @Test
    void getHandleMessageTidakTest() {
        String res = confirmDoctorState.getHandleMessage("tidak");
        String exp = "Mengganti dokter dan jadwalnya\n\n";
        Assertions.assertEquals(exp + openingMsg, res);
        Assertions.assertEquals(rsUser.getCurrentState(), confirmDoctorState.getPrevState());
    }

    @Test
    void getHandleMesageHelpTest() {
        String res = confirmDoctorState.getHandleMessage("help");

        Assertions.assertEquals(String.format(helpMsg)
                + openingMsg, res);
    }
}
