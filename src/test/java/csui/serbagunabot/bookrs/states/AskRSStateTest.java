package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AskRSStateTest {
    RSUser rsUser;
    AskRSState askRSState;

    /**
     * Sets up the rsUser, the next state, and the init state.
     */
    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
        this.askRSState = new AskRSState(rsUser);
        askRSState.setNextState(new AskDepartmentState(rsUser));
        askRSState.setInitState(new RSInitialState(rsUser));
    }

    @Test
    void testHandleMsg() {
        String res = askRSState.getHandleMessage("1");
        Assertions.assertEquals("RS A", rsUser.getRumahSakit());

        res = askRSState.getHandleMessage("2");
        Assertions.assertEquals("RS B", rsUser.getRumahSakit());

        res = askRSState.getHandleMessage("3");
        Assertions.assertEquals("RS C", rsUser.getRumahSakit());

        Assertions.assertEquals(String.format("Anda memilih RS C %n%n")
                + "Silahkan pilih Departemen dokter yang "
                + "diinginkan dengan cara mengetik:\n"
                + " '!doctor [Nomor Departemen]'(Contoh: !doctor 1)\n\n"
                + "List Departemen yang tersedia:\n"
                + "1. Umum\n2. Gigi dan Bedah Mulut\n3. THT\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), askRSState.getNextState());
    }

    @Test
    void testHandleMsgNotFound() {
        String res = askRSState.getHandleMessage("TEST");

        Assertions.assertEquals("Nama RS tidak ditemukan!\n"
                + "Ketik !doctor untuk mulai menggunakan fitur Booking RS", res);
        Assertions.assertNotEquals(rsUser.getCurrentState(), askRSState.getNextState());
    }

    @Test
    void testOpeningMsg() {
        String res = askRSState.getOpeningMessage();

        Assertions.assertEquals("Silahkan pilih Rumah Sakit yang "
                + "diinginkan dengan cara mengetik:\n"
                + " '!doctor [Nomor RS]'(Contoh: !doctor 1)\n\nList RS yang tersedia:\n"
                + "1. RS A\n2. RS B\n3. RS C\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }

    @Test
    void testHandleMessageHelp() {
        String res = askRSState.getHandleMessage("help");

        Assertions.assertEquals("Anda dalam tahap memilih Rumah Sakit\n\n"
                + "Silahkan pilih Rumah Sakit yang "
                + "diinginkan dengan cara mengetik:\n"
                + " '!doctor [Nomor RS]'(Contoh: !doctor 1)\n\nList RS yang tersedia:\n"
                + "1. RS A\n2. RS B\n3. RS C\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }
}
