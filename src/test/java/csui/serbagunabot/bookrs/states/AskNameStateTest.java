package csui.serbagunabot.bookrs.states;

import csui.serbagunabot.bookrs.core.RSUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AskNameStateTest {
    RSUser rsUser;
    AskNameState askNameState;

    private final String openingMsg = "Silahkan masukkan Nama Pasien "
            + "dengan format:\n'!doctor [Nama Pasien]' "
            + "contoh: !doctor Budi Dubi\n\n"
            + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'";

    private final String invalidNameMsg = "Nama yang diberikan tidak valid!\n"
            + "Mohon memasukkan nama tanpa angka ataupun simbol!\n\n";

    /**
     * Sets up the rsUser, the next state, and the init state.
     */
    @BeforeEach
    void setUp() {
        this.rsUser = new RSUser();
        this.askNameState = new AskNameState(rsUser);
        askNameState.setNextState(new ConfirmNameState(rsUser));
        askNameState.setInitState(new RSInitialState(rsUser));
    }

    @Test
    void testGetOpeningMessage() {
        String res = askNameState.getOpeningMessage();

        Assertions.assertEquals("Silahkan masukkan Nama Pasien "
                + "dengan format:\n'!doctor [Nama Pasien]' "
                + "contoh: !doctor Budi Dubi\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
    }

    @Test
    void testGetHandleMsgNotFound() {
        String res = askNameState.getHandleMessage("");
        Assertions.assertEquals("Tidak memasukkan nama pasien!\n"
                + "Ketik !doctor untuk mulai menggunakan fitur Booking RS", res);
        Assertions.assertNotEquals(rsUser.getCurrentState(), askNameState.getNextState());
    }

    @Test
    void testGetHandleMsgValidName() {
        String res = askNameState.getHandleMessage("dummy");

        Assertions.assertEquals("dummy", rsUser.getNamaPasien());
        Assertions.assertEquals(String.format("Nama Pasien: dummy %n%n")
                + "Apakah nama pasien yang tertera sudah benar?\n"
                + "Jika sudah benar, masukkan Ya. Jika belum, masukkan Tidak\n"
                + "Tuliskan konfirmasi dengan format:\n!doctor [Ya/Tidak] "
                + "Contoh: !doctor Ya\n\n"
                + "Jika membutuhkan bantuan maka ketik:\n'!doctor help'", res);
        Assertions.assertEquals(rsUser.getCurrentState(), askNameState.getNextState());
    }

    @Test
    void testGetHandleMsgInvalidName() {
        String res = askNameState.getHandleMessage("Dummy Dummy1");

        Assertions.assertNull(rsUser.getNamaPasien());
        Assertions.assertEquals(invalidNameMsg + openingMsg, res);
    }

    @Test
    void testGetHandleMsgHelp() {
        String res = askNameState.getHandleMessage("help");

        Assertions.assertEquals("Anda dalam tahap mengisi Nama Pasien\n\n"
                + openingMsg, res);
    }
}
