package csui.serbagunabot.bookrs.service;

import csui.serbagunabot.bookrs.core.RSUser;
import csui.serbagunabot.bookrs.repository.RSUserRepository;
import csui.serbagunabot.bookrs.states.RSInitialState;
import csui.serbagunabot.bookrs.states.RSStateGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookRSHandlerServiceTest {

    @InjectMocks
    private BookRSHandlerService bookRSHandlerService;


    @Mock
    private RSUserRepository rsUserRepository;

    private RSUser rsUser;
    private RSInitialState rsInitialState;
    private static final String MESSAGE_1 = "!doctor";
    private static final String USER_ID = "basyira";

    @BeforeEach
    public void setUp() {
        rsUser = new RSUser();
        rsInitialState = new RSInitialState(rsUser);
        rsUser.setCurrentState(rsInitialState);
        rsInitialState.setNextState(rsInitialState);
    }

    @Test
    void testMessageHandlerShouldReturnUserIfUserIsFound() {
        when(rsUserRepository.findById(USER_ID)).thenReturn(rsUser);
        String res = bookRSHandlerService.handleMessage(MESSAGE_1, USER_ID);
        Assertions.assertNotNull(res);
        verify(rsUserRepository, times(0)).createUserFromId("TEST");
    }

    @Test
    void testMessageHandleUserNotExist() {
        when(rsUserRepository.createUserFromId(USER_ID)).thenReturn(rsUser);

        String res = bookRSHandlerService.handleMessage(MESSAGE_1, USER_ID);

        Assertions.assertNotNull(res);
        verify(rsUserRepository, times(1)).createUserFromId(USER_ID);
    }
}
