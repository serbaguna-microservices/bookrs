package csui.serbagunabot.bookrs.repository;

import csui.serbagunabot.bookrs.core.RSUser;
import csui.serbagunabot.bookrs.states.RSStateGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RSUserRepositoryTest {

    @InjectMocks
    RSUserRepositoryImpl rsUserRepository;

    @Mock
    RSStateGenerator rsStateGenerator;

    private RSUser rsUser;

    @Test
    void testCreateByIdSuccess() {
        rsUser = rsUserRepository.createUserFromId("TEST");
        Assertions.assertNotNull(rsUser);
    }

    @Test
    void testFindByIdSuccess() {
        rsUserRepository.createUserFromId("TEST");
        rsUser = rsUserRepository.findById("TEST");
        Assertions.assertNotNull(rsUser);
    }

    @Test
    void testFindByIdNotExist() {
        rsUser = rsUserRepository.findById("TEST");
        Assertions.assertNull(rsUser);
    }
}