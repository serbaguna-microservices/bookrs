package csui.serbagunabot.bookrs.repository;

import csui.serbagunabot.bookrs.BookRSInitializer;
import csui.serbagunabot.bookrs.core.RSDoctor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RSDoctorRepositoryTest {

    BookRSInitializer bookRSInitializer;
    RSDoctorRepository rsDoctorRepository;

    private RSDoctor rsDoctor;

    @BeforeEach
    void setUp() {
        bookRSInitializer = new BookRSInitializer();
        bookRSInitializer.init();
        this.rsDoctorRepository = RSDoctorRepositoryImpl.getDoctorRepository();
    }

    @Test
    void testFindByIdSuccess() {
        rsDoctor = rsDoctorRepository.findByKode("AU1");
        Assertions.assertNotNull(rsDoctor);
    }
}
