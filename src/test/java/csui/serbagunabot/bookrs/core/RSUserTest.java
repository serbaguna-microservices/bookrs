package csui.serbagunabot.bookrs.core;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import csui.serbagunabot.bookrs.states.RSInitialState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RSUserTest {

    @Mock
    RSInitialState rsInitialState;

    private RSUser rsUser;

    @BeforeEach
    public void setUp() {
        this.rsUser = new RSUser();
    }

    @Test
    void testSetRumahSakit() {
        rsUser.setRumahSakit("TEST");
        Assertions.assertEquals("TEST", rsUser.getRumahSakit());
    }

    @Test
    void testSetDepartemen() {
        rsUser.setDepartemen("TEST");
        Assertions.assertEquals("TEST", rsUser.getDepartemen());
    }

    @Test
    void testSetNamaPasien() {
        rsUser.setNamaPasien("TEST");
        Assertions.assertEquals("TEST", rsUser.getNamaPasien());
    }

    @Test
    void testSetNamaDokter() {
        rsUser.setNamaDokter("TEST");
        Assertions.assertEquals("TEST", rsUser.getNamaDokter());
    }

    @Test
    void testSetRSDoctor() {
        RSDoctor rsDoctor = new RSDoctor();
        rsUser.setRsDoctor(rsDoctor);
        RSDoctor res = rsUser.getRsDoctor();
        Assertions.assertNotNull(res);
    }

    @Test
    void testHandleMessage() {
        rsUser.setCurrentState(rsInitialState);
        rsUser.respond("TEST");
        verify(rsInitialState, times(1)).getHandleMessage("TEST");
    }
}
