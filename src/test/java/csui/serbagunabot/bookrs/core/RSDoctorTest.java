package csui.serbagunabot.bookrs.core;

import java.time.DayOfWeek;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RSDoctorTest {
    private RSDoctor rsDoctor;

    @BeforeEach
    public void setUp() {
        rsDoctor = new RSDoctor("Dummy", "Dummy", "Dummy",  "Dummy", "Dummy",
                DayOfWeek.FRIDAY, DayOfWeek.SATURDAY);
    }

    @Test
    void testGetterSetterRS() {
        rsDoctor.setRs("Dummy1");
        Assertions.assertEquals("Dummy1", rsDoctor.getRs());
    }

    @Test
    void testGetterSetterDept() {
        rsDoctor.setDept("Dummy1");
        Assertions.assertEquals("Dummy1", rsDoctor.getDept());
    }
}

