package csui.serbagunabot.bookrs.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.serbagunabot.bookrs.model.line.LineRequest;
import csui.serbagunabot.bookrs.service.BookRSHandlerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class BookRSControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    BookRSHandlerService bookRSHandlerService;

    LineRequest lineRequest;

    public static final String SAMPLE_RESPONSE = "Test Response";
    public static final String SAMPLE_USERID = "basyira";
    public static final String SAMPLE_MSG = "Test";

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @BeforeEach
    void setUp() {
        lineRequest = new LineRequest();
        lineRequest.setMessage(SAMPLE_MSG);
        lineRequest.setUserId(SAMPLE_USERID);
    }

    @Test
    void testReplyEntrypointBookRSShouldReturnJson() throws Exception {
        when(bookRSHandlerService
            .handleMessage(anyString(), anyString()))
            .thenReturn(SAMPLE_RESPONSE);

        mvc.perform(post("/")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mapToJson(lineRequest)))
            .andExpect(jsonPath("$.response")
            .value(SAMPLE_RESPONSE));
    }
}
